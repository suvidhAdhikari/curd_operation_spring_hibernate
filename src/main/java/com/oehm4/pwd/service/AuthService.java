package com.oehm4.pwd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oehm4.pwd.dao.AuthDAO;
import com.oehm4.pwd.dto.LoginDTO;
import com.oehm4.pwd.entity.Register;

@Service
public class AuthService
{
	@Autowired
	AuthDAO authDAO;
	public AuthService() 
	{
	// TODO Auto-generated constructor stub
	}
	public void saveRegisterDetails(Register register)
	{
		authDAO.saveRegisterDetails(register);
	}
	public Register getRegisterDetailsByLoginDto(LoginDTO loginDTO)
	{
		return authDAO.getRegisterDetailsByLoginDto(loginDTO);
	}
	public Register getRegisterDetailsByEmailId(String Email)
	{
		return authDAO.getRegisterDetailsByEmailId(Email);
	}
	
}
