package com.oehm4.pwd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oehm4.pwd.dao.AppUserDAO;
import com.oehm4.pwd.entity.AppUserDetails;
import com.sun.org.apache.regexp.internal.recompile;
@Service
public class AppUserService
{
	@Autowired
	private AppUserDAO appUserDAO;
	
	public AppUserService() {
		System.out.println(this.getClass().getSimpleName()  +" created");
	}
	
	public void saveUserData(AppUserDetails appUserDetails) 
	{
		appUserDAO.saveUserData(appUserDetails);
	}
	
	public List<AppUserDetails> getAppDetailsByLogin(Long id)
	{
		
		return appUserDAO.getAppUserDetailsByLogin(id);
	}
	public AppUserDetails getAppUserDetailsById(Long id)
	{
		return appUserDAO.getAppUserDetailsById(id);
	}
	public void updateAppUserDetails(AppUserDetails appUserDetails)
	{
		appUserDAO.updateAppUserDetails(appUserDetails);
	}
	public void deleteAppUserDetails(Long id)
	{
		appUserDAO.deleteAppUserDetails(id);
	}
	public void deleteAllAppUserDetails(Long id)
	{
		appUserDAO.deleteAllAppUserDetails(id);
	}
}
