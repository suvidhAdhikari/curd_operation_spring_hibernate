package com.oehm4.pwd.entity;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
@Table(name = "register_customer_details")
public class Register implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2039407221792618665L;

	@Id
	@GenericGenerator(name = "register_auto",strategy = "increment")
	@GeneratedValue(generator = "register_auto")
	@Column(name = "rid")
	private Long id;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "email",unique=true)
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name="contact_number")
	private String contactNumbner;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "pincode")
	private String pincode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getContactNumbner() {
		return contactNumbner;
	}

	public void setContactNumbner(String contactNumbner) {
		this.contactNumbner = contactNumbner;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "Register [id=" + id + ", userName=" + userName + ", email=" + email + ", password=" + password
				+ ", contactNumbner=" + contactNumbner + ", city=" + city + ", country=" + country + ", pincode="
				+ pincode + "]";
	}
	

}
