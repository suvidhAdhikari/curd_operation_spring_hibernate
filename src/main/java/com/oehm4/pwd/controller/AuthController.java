package com.oehm4.pwd.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.oehm4.pwd.dto.LoginDTO;
import com.oehm4.pwd.entity.AppUserDetails;
import com.oehm4.pwd.entity.Register;
import com.oehm4.pwd.service.AppUserService;
import com.oehm4.pwd.service.AuthService;



@Controller
@RequestMapping("/")
public class AuthController 
{
	@Autowired
	private AuthService authService;
	
	@Autowired
	private AppUserService appUserService;

	public AuthController(){
	}
	@RequestMapping("/saveUserData")
	public ModelAndView saveRegisterDetails(Register register)
	{
		Register emailId = authService.getRegisterDetailsByEmailId(register.getEmail());
		if(emailId!=null)
		{
			return new ModelAndView("register","msg","email already in use!");
		}
		else 
		{
		authService.saveRegisterDetails(register);
		return new ModelAndView("login","msg","Registraion successfull please login!");
		}
	
	}
	@RequestMapping("/loginVailidation")
	public ModelAndView getRegisterDetailsByLoginDto(LoginDTO loginDTO,HttpServletRequest request)
	{
		Register register = authService.getRegisterDetailsByLoginDto(loginDTO);
		if(register != null) {
			HttpSession session = request.getSession();
			session.setAttribute("register", register);
			List<AppUserDetails> list = appUserService.getAppDetailsByLogin(register.getId());
			list.forEach(a->{
				System.out.println(a);
			});
			return new ModelAndView("home","list",list);
		}
		return new ModelAndView("login","msg","Invalid credentials!");
	}
	
}
