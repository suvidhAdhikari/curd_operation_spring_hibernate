package com.oehm4.pwd.controller;

import java.util.List;  

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.oehm4.pwd.entity.AppUserDetails;
import com.oehm4.pwd.entity.Register;
import com.oehm4.pwd.service.AppUserService;


@Controller
@RequestMapping(value = "/")
public class AppUserController {

	@Autowired
	private AppUserService appUserService;
	
	public AppUserController() {
	}
	
	@RequestMapping(value = "/home")
	public ModelAndView home(HttpServletRequest request)
	{
		Register register = (Register) request.getSession().getAttribute("register");
		List<AppUserDetails> list = appUserService.getAppDetailsByLogin(register.getId());
		return new ModelAndView("home","list",list);
	}
	@RequestMapping(value = "/saveAppUserData")
	public  String saveUserData(AppUserDetails appUserDetails,HttpServletRequest request) {
		System.out.println(appUserDetails);
		Register register = (Register) request.getSession().getAttribute("register");
		appUserDetails.setRegister(register);
		System.out.println(appUserDetails);
		appUserService.saveUserData(appUserDetails);
		return "redirect:/home";

	}
	 @RequestMapping(value="/editemp/{id}")    
	public ModelAndView getUserDetails(@PathVariable Long id)
	{	
		AppUserDetails appUserDetailsById = appUserService.getAppUserDetailsById(id); 
		System.out.println(appUserDetailsById);
		return new ModelAndView("update","p",appUserDetailsById);
	}
	 @RequestMapping(value="/update")   
	 public String updateAppUserDetails(AppUserDetails appUserDetails,HttpServletRequest request)
	 {
		 Register register = (Register) request.getSession().getAttribute("register");
		 if(register!=null){
		 appUserDetails.setRegister(register);
		 System.out.println(appUserDetails);
		 appUserService.updateAppUserDetails(appUserDetails);
		 return "redirect:/home";
		 }
		 return "home";
	 }
	 @RequestMapping(value="/delete/{id}")    
	public String deleteAppUserDetail(@PathVariable Long id,HttpServletRequest request)
	{	
		 System.out.println(id);
		 appUserService.deleteAppUserDetails(id);
		 return "redirect:/home";
	}
	 @RequestMapping(value="/deleteData")    
	 public String deleteAllAppUserDetails(HttpServletRequest request)
	 { 
		 Register register = (Register) request.getSession().getAttribute("register");
		 appUserService.deleteAllAppUserDetails(register.getId());
		 return "redirect:/home";
	 }
	
	 
}