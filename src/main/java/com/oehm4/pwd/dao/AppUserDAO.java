package com.oehm4.pwd.dao;

import java.util.List; 

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.oehm4.pwd.entity.AppUserDetails;

@Repository
public class AppUserDAO 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public AppUserDAO() {
	}
	
	public void saveUserData(AppUserDetails appUserDetails) 
	{
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try 
		{
			session.save(appUserDetails);
			transaction.commit();
		}
		catch (Exception e)
		{
			transaction.rollback();
			System.out.println("saving opreation");
		}
		finally 
		{
			session.close();
		}
	}
	
	public List<AppUserDetails> getAppUserDetailsByLogin(Long id) 
	{
		Session session = sessionFactory.openSession();
		String hql ="from AppUserDetails where register.id=:i";
		Query query = session.createQuery(hql);
		query.setCacheable(true);
		query.setParameter("i", id);
		List<AppUserDetails> list = query.list();
		return list;
	}
	public AppUserDetails getAppUserDetailsById(Long id)
	{
		Session session = sessionFactory.openSession();
		AppUserDetails appUserDetails = session.get(AppUserDetails.class, id);
		if (appUserDetails!=null)
		{
			session.close();
			return appUserDetails;
		}
		else
		{
			System.out.print("there is no data available for given id :"+id);
			return null;
		}
		
	}
	
	public void updateAppUserDetails(AppUserDetails appUserDetails)
	{
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try 
		{
			session.update(appUserDetails);
			transaction.commit();
		}
		catch (Exception e) 
		{
			transaction.rollback();
			System.err.println("update operation failed");
		}
		finally 
		{
			session.close();
		}
	}
	public void deleteAppUserDetails(Long id)
	{
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try 
		{
			String hql="delete from AppUserDetails where id=:a";
			Query query = session.createQuery(hql);
			query.setCacheable(true);
			query.setParameter("a", id);
			query.executeUpdate();
			transaction.commit();
		}
		catch (Exception e) 
		{
			transaction.rollback();
			System.err.println("delete operation failed");
		}
		finally 
		{
			session.close();
		}
	
	}
	public void deleteAllAppUserDetails(Long id)
	{
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try 
		{
			System.out.println("inside delete");
			String hql="delete from AppUserDetails where register.id=:i";
			Query query = session.createQuery(hql);
			query.setCacheable(true);
			query.setParameter("i",id);
			query.executeUpdate();
			transaction.commit();
		}
		catch (Exception e) 
		{
			transaction.rollback();
			System.err.println("delete operation failed");
		}
		finally 
		{
			session.close();
		}
	}
}
