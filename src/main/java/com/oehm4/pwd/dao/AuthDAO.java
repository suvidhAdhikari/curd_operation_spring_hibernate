package com.oehm4.pwd.dao;

import org.hibernate.Query;  
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.oehm4.pwd.dto.LoginDTO;
import com.oehm4.pwd.entity.Register;
@Repository
public class AuthDAO 
{
	private static final String Register = null;
	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveRegisterDetails(Register register)
	{
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(register);
			transaction.commit();
		}
		catch (Exception e) 
		{
			System.out.println("save operation failed");
			transaction.rollback();
		}
	}
	
	public Register getRegisterDetailsByLoginDto(LoginDTO loginDTO)
	{
		Session session = sessionFactory.openSession();
		String hql="from Register where email=:a and password=:b";
		Query query = session.createQuery(hql);
		query.setCacheable(true);
		query.setParameter("a", loginDTO.getEmail());
		query.setParameter("b", loginDTO.getPassword());
		Register register = (Register) query.uniqueResult();
		return register;
	}
	public Register getRegisterDetailsByEmailId(String Email)
	{
		Session session = sessionFactory.openSession();
		String hql="from Register where email=:a";
		Query query = session.createQuery(hql);
		query.setCacheable(true);
		query.setParameter("a", Email);
		Register register = (Register) query.uniqueResult();
		return register;
	}
	
	
}
