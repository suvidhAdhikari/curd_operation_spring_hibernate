<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>home</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
	<!-- Material Design Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    
<style type="text/css">
body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    background: linear-gradient(to right, #b92b27, #1565c0)
}
.table td
{
vertical-align: middle;
}
</style>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript">
      
      	</script>
</head>
<body>
<div class="text-center mt-5 row">
	<a type="button" href="${pageContext.request.contextPath}/userDetails.jsp" class="btn btn-success ml-5 col-4 ">create</a>
	<div class="col-3"></div>
	<button class="btn btn-danger my-2 col-4" data-toggle="modal" data-target=".bs-example-modal-sm1">delete all</button>
</div>
<div class="modal bs-example-modal-sm1" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-sm">
	    	<div class="modal-content">
	      		<div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure you want to delete all data?</div>
	      		<div class="modal-footer">
	      		<a href="${pageContext.request.contextPath}/deleteData" class="btn btn-dark  text-danger font-weight-bold">delete</a>
	      		<a href="${pageContext.request.contextPath}/home" class="btn btn-dark  text-danger font-weight-bold mr-2">cancel</a>
	      		</div>
	    	</div>
	  	</div>
	</div>
  <table class="table table-dark table-bordered border-success text-center  mt-3">
	<thead class="text-danger font-weight-bold">
		<tr>
			<th colspan="7" class="font-weight-bold text-success">password Management system</th>
		</tr>
		<tr>
			<th class="font-weight-bold">Application Name</th>
			<th class="font-weight-bold">Email</th>
			<th class="font-weight-bold">UserName</th>
			<th class="font-weight-bold">Password</th>
			<th colspan="2" class="font-weight-bold ">Actions</th>
		</tr>
	</thead>
	<tbody>
 		<c:forEach items="${list}" var="app">
 			<tr class="my-auto">
 				<td>${app.appName}</td>
 				<td>${app.email}</td>
 				<td>${app.userName}</td>
 				<td>${app.password}</td>
 				<td><a type="button" href="${pageContext.request.contextPath}/editemp/${app.id}" class="btn btn-success w-100">Edit</a></td>
 				<td><a type="button" href="${pageContext.request.contextPath}/delete/${app.id}" class="btn btn-success w-100">delete</a></td>
 			</tr>
 	</c:forEach>
	</tbody>
</table>
 	<div class="text-center">
 	<button class="btn btn-dark text-warning col-4" data-toggle="modal" data-target=".bs-example-modal-sm">Logout</button>
 	</div>
	<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-sm">
	    	<div class="modal-content">
	      		<div class="modal-header"><h4>Logout <i class="fa fa-lock"></i></h4></div>
	      		<div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure you want to log-out?</div>
	      		<div class="modal-footer"><a href="${pageContext.request.contextPath}/login.jsp" class="btn btn-dark btn-block text-danger font-weight-bold">Logout</a></div>
	    	</div>
	  	</div>
	</div>
</body>
</html>
